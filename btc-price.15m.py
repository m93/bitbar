#!/usr/bin/python
# coding=utf-8
#
# <bitbar.title>Bitcoin Ticker (PLN)</bitbar.title>
# <bitbar.version>v1.0</bitbar.version>
# <bitbar.author>mcimasz</bitbar.author>
# <bitbar.desc>Displays current Bitcoin price for PLN from Bitmarket / Bitbay</bitbar.desc>
#
# Credits to: impshum

import json
from urllib import urlopen

#url = urlopen('https://www.bitmarket.pl/json/BTCPLN/ticker.json').read()
url = urlopen('https://bitbay.net/API/Public/BTCPLN/ticker.json').read()

result = json.loads(url)
last = result['last']
vwap = result['vwap']

def flow():
	# last transaction was equal 1% (or higher) over average from last 24h
    if last > vwap:
        print ('Sell >=0.1 - %.2f'% float(last))
    # last transaction was equal to average from last 24h
    elif last == vwap:
    	print ('Wait - %.2f'% float(last))
    # last transaction was smaller than average from last 24h
    else:
        print ('Buy - %.2f'% float(last))

flow()
