#!/usr/bin/python
# coding=utf-8
#
# <bitbar.title>Crypto Ticker (BTC)</bitbar.title>
# <bitbar.version>v1.0</bitbar.version>
# <bitbar.author>mcimasz</bitbar.author>
# <bitbar.desc>Displays few crypto price in BTC from Poloniex</bitbar.desc>
#
# Credits to: impshum

import json
from urllib import urlopen

url = urlopen('https://poloniex.com/public?command=returnTicker').read()
result = json.loads(url)

cc = [result['BTC_ETH'], result['BTC_XMR'], result['BTC_XRP']]

for i in cc:
	# crypto details
	last = i['last']
	high24hr = i['high24hr']
	low24hr = i['low24hr']
	pre_vwap = [float(high24hr), float(low24hr)]
	vwap = sum(pre_vwap) / float(len(pre_vwap))
	#print i

	def flow():
		# last transaction was equal 1% (or higher) over average from last 24h
	    if last > vwap:
	        print ('Sell >=0.1 - %.5f'% float(last))
	    # last transaction was equal to average from last 24h
	    elif last == vwap:
	    	print ('Wait - %.5f'% float(last))
	    # last transaction was smaller than average from last 24h
	    else:
	        print ('Buy - %.5f'% float(last))

	flow()
